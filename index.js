import restify from "restify";
import DBConnection from "./config/database";

/**
 * Creating server
 * @type {*|Server}
 */
const server = restify.createServer({
    name: "material-service",
    ignoreTrailingSlash: true
});

/**
 * Configure restify plugins
 */
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

/**
 * Load env file
 */
require("dotenv").config();

/**
 * Registering routers
 */
const router = require("./src/Interfaces/Http/Routes/index");
router.applyRoutes(server);

/**
 * Connect to MongoDB server
 */
DBConnection();

/**
 * Starting server
 */
server.listen(process.env.PORT || 7000, () => {
    console.log(`Material Service started at port ${process.env.PORT || 7000}!`);
});