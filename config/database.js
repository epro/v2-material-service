import mongoose from "mongoose";
import tunnel from "tunnel-ssh";

require("dotenv").config({
    path: "./../.env"
});

function production() {
    const config = {
        username: process.env.SSH_USERNAME,
        host: process.env.SSH_HOST,
        privateKey: require("fs").readFileSync(process.env.SSH_PRIVATE_KEY),
        port: process.env.SSH_PORT,
        dstHost: "127.0.0.1",
        dstPort: process.env.MONGO_PORT,
        localHost: "127.0.0.1",
        localPort: process.env.MONGO_PORT
    };

    return tunnel(config, () => {
        return local();
    });
}

function local() {
    /**
     * Connect to MongoDB server
     * @namespace process.env.MONGO_URI
     */
    mongoose.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useCreateIndex: true,
    }).then(() => {
        console.log("Successfully connected to DB!");
    }, err => {
        console.log("An error occurred while connecting to DB!");
        throw new Error(err);
    });
}

module.exports = () => {
    if (process.env.APP_ENV === "local") {
        return local();
    } else {
        return production();
    }
};