import SuccessResponse from "@responses/SuccessResponse";
import NotFoundResponse from "@responses/NotFoundResponse";
import { MaterialRepository } from "@repositories/MaterialRepository";
import MaterialNotFoundException from "@exceptions/MaterialNotFoundException";
import InternalServerErrorResponse from "@responses/InternalServerErrorResponse";
import { CreateMaterialService } from "@material/Services/CreateMaterialService";
import { DeleteMaterialService } from "@material/Services/DeleteMaterialService";
import { UpdateMaterialService } from "@material/Services/UpdateMaterialService";

const RouterInstance = require('restify-router').Router;
const router = new RouterInstance;

router.get("/", async (req, res) => {
	try {
		const result = await new MaterialRepository().list(req.headers.role, {}, req.query.page, req.query.per_page).get(req.query.class_id);
		return SuccessResponse(res, "List materials", result);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(res, exception.message);
	}
});

router.get("/:id", async (req, res) => {
	try {
		const result = await new MaterialRepository().detail(req.params.id).get();
		return SuccessResponse(res, "Detail material", result);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof MaterialNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, exception.message);
		}
	}
});

router.post("/", async (req, res) => {
	try {
		req.body.created_by = req.headers.id;
		const result = await new CreateMaterialService(req.body).persist();
		return SuccessResponse(res, "Successfully create new material!", result);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(res, exception.message);
	}
});

router.put("/:id", async (req, res) => {
	try {
		const result = await new UpdateMaterialService().persist(req.params.id, req.body);
		return SuccessResponse(res, "Successfully update material!", result);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(exception.message);
	}
});

router.del("/:id", async (req, res) => {
	try {
		const result = await new DeleteMaterialService(req.params.id).persist();
		return SuccessResponse(res, "Successfully delete material!", result);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(exception.message);
	}
});

module.exports = router;