const RouterInstance = require('restify-router').Router;
const router = new RouterInstance;

router.add("/materials", require("./Materials"));

module.exports = router;