/**
 * Not found response builder
 * @param res
 * @param message
 * @param error_code
 * @returns {{success: boolean, message: *, error_code: *, data: null}}
 */
export default (res, message, error_code = 0) => {
    res.status(404);
    res.json({
        success: false,
        message: message,
        error_code: error_code,
        data: null
    });
}