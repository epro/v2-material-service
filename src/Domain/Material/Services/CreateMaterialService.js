import MaterialEntity from "@material/Entities/MaterialEntity";

export class CreateMaterialService {

	constructor (data) {
		this._data = data;
	}

	async persist () {
		return MaterialEntity.create(this._data).then(created => {
			return created;
		}).catch(error => {
			throw error;
		});
	}
}