import MaterialEntity from "@material/Entities/MaterialEntity";
import MaterialNotFoundException from "@exceptions/MaterialNotFoundException";

export class DeleteMaterialService {

	constructor (id) {
		this._id = id;
	}

	async persist () {
		return MaterialEntity.findByIdAndUpdate(this._id, {
			$set: {
				deleted_at: Date.now()
			}
		}, {
			new: true
		}).then(updated => {
			if (updated !== null) {
				return updated;
			} else {
				throw new MaterialNotFoundException("The material you're looking for couldn't be found!");
			}
		}).catch(error => {
			throw error;
		});
	}
}