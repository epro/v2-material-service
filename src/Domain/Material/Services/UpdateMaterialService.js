import MaterialEntity from "@material/Entities/MaterialEntity";
import MaterialNotFoundException from "@exceptions/MaterialNotFoundException";

export class UpdateMaterialService {

	constructor (id, data) {
		this._id = id;
		this._data = data;
	}

	async persist () {
		return MaterialEntity.findByIdAndUpdate(this._id, {
			$set: this._data
		}, {
			new: true
		}).then(updated => {
			if (updated !== null) {
				return updated;
			} else {
				throw new MaterialNotFoundException("The material you're looking for couldn't be found!");
			}
		}).catch(error => {
			throw error;
		});
	}
}