import mongoose from "mongoose";

const Schema = mongoose.Schema;

/**
 * Defining schema
 */
const materi = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    class_id: {
        type: Schema.Types.ObjectId,
        required: true
    },
    material_body: String,
    created_by: {
        type: String,
        required: Schema.Types.ObjectId
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    },
    deleted_at: {
        type: Date,
        default: null
    }
});

/**
 * Creating classes model
 * @type {Model}
 */
const Materies = mongoose.model("Materies", materi);

/**
 * Exporting classes model
 * @type {Model}
 */
module.exports = Materies;