import rp from "request-promise";
import MaterialEntity from "@material/Entities/MaterialEntity";
import { InvalidOptionException } from "@exceptions/InvalidOptionException";
import { MaterialNotFoundException } from "@exceptions/MaterialNotFoundException";

export class MaterialRepository {

	list (option, param = {}, page = 1, per_page = 10) {
		this._for = option;
		this._param = param;
		this._page = page;
		this.per_page = per_page;
		this._state = "list";
		return this;
	}

	detail (id) {
		this._id = id;
		this._state = "detail";
		return this;
	}

	async admin () {
		return MaterialEntity.find({})
			.select("-__v -material_body -updated_at -deleted_at")
			.skip(this._per_page * (this._page - 1))
			.limit(this._per_page)
			.lean();
	}

	async byClass (class_id) {
		return MaterialEntity.find({ class_id: class_id })
			.select("-__v -material_body -updated_at -deleted_at")
			.skip(this._per_page * (this._page - 1))
			.limit(this._per_page)
			.lean();
	}

	async materialById () {
		return MaterialEntity.findById(this._id).select("-__v -updated_at -deleted_at").lean().then(material => {
			if (material !== null) {
				return material;
			} else {
				throw new MaterialNotFoundException("The material you're looking for couldn't be found!");
			}
		}).catch(error => {
			throw error;
		});
	}

	async get (class_id = null) {
		switch (this._state) {
			case "list" :
				if (this._for === "ADMIN") this._materials = await this.admin();
				if (this._for === "STUDENT") this._materials = await this.byClass(class_id);
				await this.fillClassesData();
				// await this.fillTeachersData();
				return this._materials;
				break;
			case "detail":
				this._material = await this.materialById(this._id);
				await this.fillClassData();
				// await this.fillTeacherData();
				return this._material;
				break;
			default:
				throw new Error("Invalid state!");
		}
	}

	async fillClassData () {
		const classes = await rp(`${process.env.CLASS_SERVICE_URI}/class/${this._material.class_id}`, { json: true });
		if (classes !== null) this._material.class_name = classes.data.name;
		return this;
	}

	async fillClassesData () {
		const classes = await rp(`${process.env.CLASS_SERVICE_URI}/class`, { json: true });
        const promises = [];
        this._materials.forEach(material => {
        	promises.push(
        		classes.data.forEach(cls => {
        			if (cls._id === `${material.class_id}`) material.class_name = cls.name;
        		})
        	);
        });
    
        return Promise.all(promises).then(() => {
        	return this;
        });
	}

	async fillTeachersData () {
		const teachers = await rp(`${process.env.AUTH_SERVICE_URI}/teacher/all`, { json: true });
        const promises = [];
        this._materials.forEach(material => {
        	promises.push(
        		teachers.data.forEach(teacher => {
        			if (teacher._id === material.created_by) material.teacher_name = teacher.name;
        		})
        	);
        });
    
        return Promise.all(promises).then(() => {
        	return this;
        });
	}
}